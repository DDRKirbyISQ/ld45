extends KinematicBody2D

class_name Player

const hurtSound:AudioStream = preload("hurt.wav")
const landSound:AudioStream = preload("land.wav")
const deathSound:AudioStream = preload("death.wav")
const hurtFilteredSound:AudioStream = preload("hurt-filtered.wav")
const deathFilteredSound:AudioStream = preload("death-filtered.wav")
const shootSound:AudioStream = preload("shoot.wav")
const jumpSound:AudioStream = preload("jump.wav")

export var bullet:PackedScene
export var deathExplosion:PackedScene

const BULLET_SPEED:float = 200.0

export var moveSpeed:float = 50
export var hitSpeed:float = 15
export var gravity:float = 475
export var fallAnimThreshold:float = 50
export var jumpVelocity:float = -160
export var shootFrames:int = 20
export var maxHealth:int = 20
export var hurtInvincibilityFrames:int = 90
export var hurtAnimFrames:int = 30

var shootAnimTimer:int = 0
var hurtTimer:int = 0
var yVelocity:float = 0
var health:int
var dead:bool = false

var liveBullets:Array = []

func hit(damage):
	if dead:
		return
	if hurtTimer > 0:
		return

	if !Unlocks.isUnlocked("health"):
		_setHealth(0)

	_setHealth(health - damage)
	if health <= 0:
		kill()
		return
	hurtTimer = hurtInvincibilityFrames
	if Unlocks.isUnlocked("game_hearing"):
		AudioPlayer.playSound(hurtSound, 1.2)
	else:
		AudioPlayer.playSound(hurtFilteredSound, 1)
	yVelocity = max(yVelocity, 0)

func kill():
	if dead:
		return
	dead = true
	_setHealth(0)
	visible = false
	Events.emit_signal("player_death")
	var explosion = deathExplosion.instance()
	if Unlocks.isUnlocked("game_hearing"):
		AudioPlayer.playSound(deathSound, 1.15)
	else:
		AudioPlayer.playSound(deathFilteredSound, 1.15)
	get_parent().add_child(explosion);
	explosion.global_position = global_position;
	explosion.emitting = true

func _ready():
	Globals.player = self
	_setHealth(maxHealth)
	# Make sure we're flush against a floor
	move_and_collide(Vector2(0, 2))
	$AnimatedSprite.play("idle")

func xDirection() -> int:
	return -1 if $AnimatedSprite.flip_h else 1

func _physics_process(delta:float) -> void:
	if dead:
		return
	if Globals.screenTransitioning:
		return

	if OS.is_debug_build():
		if Input.is_action_just_pressed("die"):
			kill()

	_applyGravity(delta)
	shootAnimTimer -= 1
	hurtTimer -= 1

	if hurtTimer > 0:
		$AnimatedSprite.visible = (hurtTimer % 2 == 0)
		if hurtTimer > hurtInvincibilityFrames - hurtAnimFrames:
			$AnimatedSprite.visible = true
			$AnimatedSprite.play("hurt")
			var xMovement = delta * hitSpeed * -xDirection()
			move_and_collide(Vector2(xMovement, 0))
			return
	else:
		$AnimatedSprite.visible = true

	if _onFloor() && Input.is_action_just_pressed("jump") && Unlocks.isUnlocked("jump"):
		yVelocity = jumpVelocity
		AudioPlayer.playSound(jumpSound, 1.5)

	if Unlocks.isUnlocked("move_right") && Input.is_action_pressed("right") && !Input.is_action_pressed("left"):
		move_and_collide(Vector2(delta * moveSpeed, 0))
		if _onFloor():
			$AnimatedSprite.play("runshoot" if shootAnimTimer > 0 else "run")
		$AnimatedSprite.flip_h = false
	elif Unlocks.isUnlocked("move_left") && Input.is_action_pressed("left") && !Input.is_action_pressed("right"):
		move_and_collide(Vector2(-delta * moveSpeed, 0))
		if _onFloor():
			$AnimatedSprite.play("runshoot" if shootAnimTimer > 0 else "run")
		$AnimatedSprite.flip_h = true
	elif _onFloor():
		$AnimatedSprite.play("shoot" if shootAnimTimer > 0 else "idle")

	if !_onFloor():
		$AnimatedSprite.play(("jump" if shootAnimTimer <= 0 else "jumpshoot") if yVelocity < fallAnimThreshold else ("fall" if shootAnimTimer <= 0 else "fallshoot"))

	var shootPressed:bool = false
	if Unlocks.isUnlocked("autofire"):
		shootPressed = Input.is_action_pressed("shoot")
	else:
		shootPressed = Input.is_action_just_pressed("shoot")
	if shootPressed && Unlocks.isUnlocked("shoot"):
		_shoot()

func _applyGravity(delta):
	var falling = yVelocity > 0
	if !_onFloor():
		yVelocity += gravity * delta
		yVelocity = min(yVelocity, 180)
	if yVelocity < 0 && !Input.is_action_pressed("jump"):
		yVelocity = 0
	var collision = move_and_collide(Vector2(0, yVelocity * delta))
	if collision != null:
		yVelocity = 0
		if falling && Unlocks.isUnlocked("game_hearing"):
			AudioPlayer.playSound(landSound, 1.7)

func _setHealth(_health):
	health = _health
	Events.emit_signal("player_health_changed", health, maxHealth)

func _onFloor():
	return test_move(transform, Vector2(0, 1))

func deleteBullets():
	var size:int = liveBullets.size()
	for i in range(size):
		if is_instance_valid(liveBullets[size - i - 1]):
			liveBullets[size - i - 1].queue_free()
		liveBullets.remove(size - i - 1)

func _shoot():
	var size:int = liveBullets.size()
	for i in range(size):
		if !is_instance_valid(liveBullets[size - i - 1]):
			liveBullets.remove(size - i - 1)

	#var limit:int = 3
	if !Unlocks.isUnlocked("multishot"):
		if shootAnimTimer > 0:
			return
	elif Unlocks.isUnlocked("autofire"):
		if shootAnimTimer > shootFrames - 8:
			return
	else:
		if shootAnimTimer > shootFrames - 6:
			return


		#limit = 1
	#if liveBullets.size() >= limit:
		#return

	var instance = bullet.instance()
	var relativePos = $ShootPoint.global_position - global_position
	relativePos.x *= xDirection()
	instance.position = global_position + relativePos
	instance.velocity = Vector2(xDirection() * BULLET_SPEED, 0)
	get_parent().add_child(instance)
	shootAnimTimer = shootFrames
	if Unlocks.isUnlocked("autofire"):
		AudioPlayer.playSound(shootSound, 0.85)
	else:
		AudioPlayer.playSound(shootSound, 0.9)
	liveBullets.push_back(instance)

func recharge():
	_setHealth(maxHealth)
