extends Enemy

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func die():
	var explosion = Explosion.instance()
	get_parent().get_parent().get_parent().add_child(explosion)
	explosion.global_position = global_position;
	explosion.emitting = true;
	get_parent().get_parent().queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
