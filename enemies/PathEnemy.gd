extends PathFollow2D

export var delay:float = 1.0
export var duration:float = .5
export var timeOffset:float = 0.0

func _ready() -> void:
	Events.connect("screen_transitioned", self, "_onScreenTransitioned")
	call_deferred("_setupTween")
	$AnimatedSprite.play("default")

func _setupTween():
	var points:int = get_parent().curve.get_point_count() - 1
	var array:Array = []
	for i in range(points + 1):
		array.push_back(get_parent().curve.get_point_position(i))
	var distances:Array = []
	var totalDistance:float = 0
	for i in range(points):
		distances.push_back(array[i].distance_to(array[i+1]))
		totalDistance += array[i].distance_to(array[i+1])

	for i in range(points):
		distances[i] = distances[i] / totalDistance

	var final:Array = [0]
	for i in range(points):
		final.push_back(final[i] + distances[i])

	yield(get_tree().create_timer(timeOffset), "timeout")

	while(get_tree().paused):
		yield(get_tree(), "idle_frame")

	while true:
		for i in range(points):
			var start:float = final[i]
			var end:float = final[i + 1]
			$Tween.interpolate_property(self, "unit_offset", start, end, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.start()
			$AnimatedSprite.play("spin")
			yield($Tween, "tween_completed")
			$AnimatedSprite.play("default")
			yield(get_tree().create_timer(delay), "timeout")

func _onScreenTransitioned(screenName:String) -> void:
	$Tween.playback_speed = 1 if Level.isInScreen(self, screenName) else 0
