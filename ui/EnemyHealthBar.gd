extends ColorRect

var actualValue:int = 18

func _ready() -> void:
	Events.connect("metal_health_changed", self, "_onPlayerHealthChanged")

func _process(delta: float) -> void:
	visible = Globals.metalManActive
	$TextureProgress.value = actualValue

func _onPlayerHealthChanged(health:int, maxHealth:int) -> void:
	$TextureProgress.max_value = maxHealth
	actualValue = health