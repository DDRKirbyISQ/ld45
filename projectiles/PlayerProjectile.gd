extends Area2D

class_name PlayerProjectile

export var damage:int = 1
var velocity:Vector2

func _ready():
	self.connect("area_entered", self, "_on_Area2D_area_entered")

func _on_Area2D_area_entered(area:Area2D):
	var enemy:Enemy = area as Enemy
	if enemy != null:
		enemy.hit(damage)
		queue_free()
	var metal:MetalMan = area.get_parent() as MetalMan
	if metal != null:
		metal.hit(damage)
		queue_free()

func _physics_process(delta: float) -> void:
	position += velocity * delta