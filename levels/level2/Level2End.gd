extends Area2D

var libraImage:Texture = preload("res://dialog-box/libra.png")
var sound:AudioStream = preload("res://sfx/door.wav")
var active:bool = true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered", self, "_onBodyEntered")

func _onBodyEntered(body:PhysicsBody2D) -> void:
	if !body is Player:
		return
	if !active:
		return

	if !Unlocks.isUnlocked("level2_key"):
		yield(DialogBox.show(get_tree(), [
			"Libra: It's locked.",
		], libraImage, 1.5), "completed")
	else:
		active = false
		var texture:TextureProgress = get_parent().get_node("Door/TextureProgress")
		for i in [27, 24, 21, 18, 15, 12, 9, 6, 3, 0]:
			texture.value = i
			yield(get_tree().create_timer(0.15), "timeout")
			AudioPlayer.playSound(sound, 1.4)
			
		yield(Unlocks.unlock("level2_door"), "completed")

		get_parent().get_node("Door").queue_free()
