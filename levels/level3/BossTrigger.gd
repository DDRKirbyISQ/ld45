extends Area2D

var libraImage:Texture = preload("res://dialog-box/libra.png")
var sound:AudioStream = preload("res://sfx/door.wav")
var active:bool = true
var music:AudioStream = preload("res://music/boss.ogg")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered", self, "_onBodyEntered")
	Events.connect("metal_death", self, "_onMetalDeath")

func _onMetalDeath() -> void:
	pass



func _onBodyEntered(body:PhysicsBody2D) -> void:
	if !body is Player:
		return
	if !active:
		return

	active = false


	get_tree().paused = true
	Globals.player.deleteBullets()

	get_parent().get_node("Door").visible = true

	var texture:TextureProgress = get_parent().get_node("Door/TextureProgress")
	for i in [27, 24, 21, 18, 15, 12, 9, 6, 3, 0]:
		texture.value = 30 - i
		yield(get_tree().create_timer(0.1), "timeout")
		AudioPlayer.playSound(sound, 1.4)

	get_parent().get_node("Door/StaticBody2D/CollisionShape2D").disabled = false

	if Unlocks.isUnlocked("metal_cutscene"):
		AudioPlayer.fadeMusic(0.5)
		yield(get_tree().create_timer(0.5), "timeout")
		get_tree().paused = false
		Globals.metalManActive = true
		Events.emit_signal("metal_health_changed", 18, 18)
		AudioPlayer.fadeMusic(0, 1, false)
		AudioPlayer.playMusic(music)
		return

	AudioPlayer.fadeMusic(2, 0, true)

	yield(DialogBox.show(get_tree(), [
		"???: So...you've come.",
	], null, 1.0, true), "completed")

	yield(DialogBox.show(get_tree(), [
		"Libra: Who...are you?",
	], libraImage, 1.0, true), "completed")

	yield(DialogBox.show(get_tree(), [
		"???: You don't remember...?",
		"???: Well...you can\nconsider yourself fortunate.",
		"???: Either way, I'll put you\nout of your misery right here!",
	], null, 1.0, true), "completed")

	yield(DialogBox.show(get_tree(), [
		"Libra: Wait! I don't understand!",
	], libraImage, 1.0, true), "completed")

	yield(DialogBox.show(get_tree(), [
		"???: Enough!",
		"???: Taste my Metal Blade!",
	], null, 1.0, true), "completed")

	get_tree().paused = false

	Unlocks.unlock("metal_cutscene", false)

	Globals.metalManActive = true
	Events.emit_signal("metal_health_changed", 18, 18)
	AudioPlayer.fadeMusic(0, 1, false)
	AudioPlayer.playMusic(music)
