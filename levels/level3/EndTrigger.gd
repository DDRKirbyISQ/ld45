extends Area2D

var libraImage:Texture = preload("res://dialog-box/libra.png")
var sound:AudioStream = preload("res://sfx/door.wav")
var active:bool = true
var music:AudioStream = preload("res://music/boss.ogg")
const creditsMusic:AudioStream = preload("res://music/credits.ogg")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered", self, "_onBodyEntered")
	Events.connect("metal_death", self, "_onMetalDeath")

func _onMetalDeath() -> void:
	Globals.igtDisabled = true
	AudioPlayer.stopMusic()
	yield(get_tree().create_timer(4.0), "timeout")

	var texture:TextureProgress = get_parent().get_node("Door2/TextureProgress")
	for i in [27, 24, 21, 18, 15, 12, 9, 6, 3, 0]:
		texture.value = i
		yield(get_tree().create_timer(0.1), "timeout")
		AudioPlayer.playSound(sound, 1.4)

	get_parent().get_node("Door2/StaticBody2D/CollisionShape2D").disabled = true




func _physics_process(delta: float) -> void:
	for body in get_overlapping_bodies():
		if body == Globals.player:
			_doEnd()

func _doEnd():

	if !get_parent().get_node("Door2/StaticBody2D/CollisionShape2D").disabled:
		return


	active = false


func _onBodyEntered(body:PhysicsBody2D) -> void:
	if !body is Player:
		return
	if !active:
		return

	active = false

	Globals.igtDisabled = true

	AudioPlayer.playSound(Sounds.sweepUp)
	AudioPlayer.fadeMusic(1)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(1), "timeout")

	if Globals.timeAttackMode:
		AudioPlayer.playSound(Sounds.confirm)
		AudioPlayer.playMusic(creditsMusic)
		Unlocks.unlock("ending", false)
		Unlocks.unlock("credits", false)
		Unlocks.unlock("jukebox", false)
		yield(UnlockPopup.showTimeAttack(get_tree(), 3), "completed")
		yield(get_tree().create_timer(1), "timeout")

		AudioPlayer.fadeMusic(2)
		yield(get_tree().create_timer(2), "timeout")
		var level2:PackedScene = load("res://scenes/menu-scene.tscn")
		Globals.activeScreenName = ""
		SceneManager.changeScene(level2)
		AudioPlayer.playSound(Sounds.sweepDown)
		yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")

	else:
		AudioPlayer.playSound(Sounds.confirm)
		yield(Unlocks.unlock("ending"), "completed")
		yield(get_tree().create_timer(1), "timeout")

		var level2:PackedScene = load("res://scenes/ending.tscn")
		Globals.activeScreenName = ""
		SceneManager.changeScene(level2)
		AudioPlayer.playSound(Sounds.sweepDown)
		yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")

