extends Node

class_name Level

const TRANSITION_DURATION:float = 1.0
const playerScene:PackedScene = preload("res://player/player.tscn")
export var music:AudioStream;

var nextScreen:Area2D = null;

static func isInScreen(object:Node, screenName:String) -> bool:
	if object == null:
		return false
	if object.name == screenName:
		return true
	var parent = object.get_parent()
	return isInScreen(parent, screenName)

func _activeScreen() -> Area2D:
	for child in $Screens.get_children():
		if child.name == Globals.activeScreenName:
			return child
	return null

func _ready() -> void:
	Globals.igtDisabled = false
	Globals.metalManActive = false
	Globals.screenTransitioning = false
	Events.connect("player_death", self, "_onPlayerDeath")
	if Globals.activeScreenName == "":
		Globals.activeScreenName = $Screens.get_child(0).name
	if Globals.activeScreenName == "3-boss":
		Globals.activeScreenName = "3-7"
	Events.emit_signal("screen_transitioned", Globals.activeScreenName)

	var spawnPos:Vector2 = _activeScreen().get_node("SpawnPoint").global_position
	_addPlayer(spawnPos)

	for child in $Screens.get_children():
		var screen = child as Area2D
		screen.get_node("SpawnPoint").visible = false
		screen.connect("body_entered", self, "_on_Screen_entered", [screen])
		screen.connect("body_exited", self, "_on_Screen_exited", [screen])
		screen.connect("area_entered", self, "_on_Screen_area_entered", [screen])
		screen.connect("area_exited", self, "_on_Screen_area_exited", [screen])

	if Unlocks.isUnlocked("game_music"):
		AudioPlayer.playMusic(music)
	else:
		while !Unlocks.isUnlocked("game_music") || get_tree().paused:
			yield(get_tree(), "idle_frame")
		AudioPlayer.playMusic(music)

func _addPlayer(pos:Vector2) -> void:
	var player = playerScene.instance()
	player.global_position = pos
	add_child(player)
	var collisionShape:CollisionShape2D = _activeScreen().get_node("CollisionShape2D");
	var camera:Camera2D = player.get_node("Camera2D")
	camera.limit_left = collisionShape.global_position.x - collisionShape.shape.extents.x
	camera.limit_right = collisionShape.global_position.x + collisionShape.shape.extents.x
	camera.limit_top = collisionShape.global_position.y - collisionShape.shape.extents.y
	camera.limit_bottom = collisionShape.global_position.y + collisionShape.shape.extents.y

func _on_Screen_entered(body:PhysicsBody2D, screen:Area2D) -> void:
	var player:Player = body as Player
	if player != null:
		nextScreen = screen

func _on_Screen_exited(body:PhysicsBody2D, screen:Area2D) -> void:
	var player:Player = body as Player

	if player != null && player.dead:
		return

	# Fallback in case we exited the screen to transition in
	# Caused by screen shaking
	if nextScreen == null:
		for curScreen in $Screens.get_children():
			if curScreen.name == Globals.activeScreenName:
				continue
			for body in curScreen.get_overlapping_bodies():
				if body is Player:
					nextScreen = curScreen

	if nextScreen == screen:
		nextScreen = null
	if player != null && !player.dead && screen.name == Globals.activeScreenName && nextScreen != null:
		_doScreenTransition(player.get_node("Camera2D"))

func _doScreenTransition(camera:Camera2D) -> void:
	if Globals.player != null && Globals.player.dead:
		return

	var tween:Tween = camera.get_node("Tween")
	if tween.is_active():
		return
	var collisionShape:CollisionShape2D = nextScreen.get_node("CollisionShape2D");
	camera.limit_left = round(camera.get_camera_position().x - 100)
	camera.limit_right = round(camera.get_camera_position().x + 100)
	camera.limit_top = round(camera.get_camera_position().y - 60)
	camera.limit_bottom = round(camera.get_camera_position().y + 60)
	camera.drag_margin_left = 1
	camera.drag_margin_top = 1
	camera.drag_margin_bottom = 1
	camera.drag_margin_right = 1
	tween.interpolate_property(camera, "limit_left", camera.limit_left, collisionShape.global_position.x - collisionShape.shape.extents.x, TRANSITION_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(camera, "limit_right", camera.limit_right, collisionShape.global_position.x + collisionShape.shape.extents.x, TRANSITION_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(camera, "limit_top", camera.limit_top, collisionShape.global_position.y - collisionShape.shape.extents.y, TRANSITION_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(camera, "limit_bottom", camera.limit_bottom, collisionShape.global_position.y + collisionShape.shape.extents.y, TRANSITION_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	Globals.activeScreenName = nextScreen.name
	Globals.screenTransitioning = true
	yield(tween, "tween_completed")
	camera.drag_margin_left = 0
	camera.drag_margin_top = 0
	camera.drag_margin_bottom = 0
	camera.drag_margin_right = 0
	Globals.screenTransitioning = false
	Events.emit_signal("screen_transitioned", Globals.activeScreenName)

func _on_Screen_area_entered(area:Area2D, screen:Area2D) -> void:
	if area is PlayerProjectile && screen.name != Globals.activeScreenName:
		area.queue_free()
	if area is EnemyProjectile && screen.name != Globals.activeScreenName:
		area.queue_free()

func _on_Screen_area_exited(area:Area2D, screen:Area2D) -> void:
	if area is PlayerProjectile:
		area.queue_free()
	if area is EnemyProjectile:
		area.queue_free()

func _onPlayerDeath() -> void:
	AudioPlayer.stopMusic()
	yield(get_tree().create_timer(2.0), "timeout")
	yield(ScreenTransitioner.transitionOut(0.5, ScreenTransitioner.DIAMONDS), "completed")
	get_tree().paused = true
	yield(get_tree().create_timer(0.5), "timeout")
	SceneManager.reloadScene()
	get_tree().paused = false
	yield(ScreenTransitioner.transitionIn(0.5, ScreenTransitioner.DIAMONDS), "completed")

func _process(delta: float) -> void:
	$Ui/Timer.visible = Globals.timeAttackMode
	var totalTicks:int = Globals.igt1 + Globals.igt2 + Globals.igt3
	var totalMilliseconds:int = round(totalTicks * 1000 / 60.0)
	var totalSeconds:int = floor(totalMilliseconds / 1000)
	var totalMinutes:int = floor(totalSeconds / 60)
	var milliseconds:int = totalMilliseconds % 1000
	var seconds:int = totalSeconds % 60
	var minutes:int = totalMinutes
	var text:String = "%02d:%02d:%03d" % [minutes, seconds, milliseconds]
	$Ui/Timer.text = text

	if Input.is_action_pressed("cheat_1") && Input.is_action_pressed("cheat_2") && Input.is_action_pressed("cheat_3"):
		if Input.is_action_just_pressed("cheat_quit") && Globals.timeAttackMode:
			AudioPlayer.stopMusic()
			var mainScene:PackedScene = load("res://scenes/menu-scene.tscn")
			Globals.activeScreenName = ""
			SceneManager.changeScene(mainScene)
