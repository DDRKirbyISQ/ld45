extends Node

const sweepUp:AudioStream = preload("res://sfx/sweep-up.wav")
const sweepDown:AudioStream = preload("res://sfx/sweep-down.wav")
const confirm:AudioStream = preload("res://sfx/confirm.wav")
