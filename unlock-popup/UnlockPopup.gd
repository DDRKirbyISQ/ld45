extends CanvasLayer

class_name UnlockPopup

const scene:PackedScene = preload("unlock-popup.tscn")
const timeAttackScene:PackedScene = preload("time-attack-popup.tscn")
const music:AudioStream = preload("res://music/unlock.ogg")

static func formatTime(ticks:int) -> String:
	if ticks == 0:
		return ".. .. ..."

	var totalMilliseconds:int = round(ticks * 1000 / 60.0)
	var totalSeconds:int = floor(totalMilliseconds / 1000)
	var totalMinutes:int = floor(totalSeconds / 60)
	var milliseconds:int = totalMilliseconds % 1000
	var seconds:int = totalSeconds % 60
	var minutes:int = totalMinutes
	return "%02d:%02d:%03d" % [minutes, seconds, milliseconds]

static func showTimeAttack(sceneTree:SceneTree, level:int) -> void:
	sceneTree.paused = true
	var instance:CanvasLayer = timeAttackScene.instance()
	instance.pause_mode = Node.PAUSE_MODE_PROCESS
	sceneTree.get_root().add_child(instance)
	instance.get_node("CenterContainer/NinePatchRect/Text").text = "Level %d Complete" % level
	instance.get_node("CenterContainer/NinePatchRect/ShadowText").text = "Level %d Complete" % level
	var timeStats:String = ""
	timeStats = timeStats + "Level 1 - %s\n" % formatTime(Globals.igt1)
	timeStats = timeStats + "Level 2 - %s\n" % formatTime(Globals.igt2)
	timeStats = timeStats + "Level 3 - %s\n" % formatTime(Globals.igt3)
	timeStats = timeStats + "Total   - %s\n" % formatTime(Globals.igt1 + Globals.igt2 + Globals.igt3)
	instance.get_node("CenterContainer/NinePatchRect/Description").text = timeStats
	var tween = instance.get_node("Tween")
	var container = instance.get_node("CenterContainer")
	container.rect_scale = Vector2(0, 0)
	tween.interpolate_property(container, "rect_scale", Vector2(0, 0), Vector2(1, 1), .2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(sceneTree.create_timer(1.0), "timeout")
	yield(Yields.inputPressed("ui_accept"), "completed")
	yield(instance.get_tree(), "idle_frame")
	tween.interpolate_property(container, "rect_scale", Vector2(1, 1), Vector2(0, 0), .2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	instance.queue_free()
	sceneTree.paused = false

static func show(sceneTree:SceneTree, definition:Dictionary) -> void:
	if Globals.timeAttackMode:
		yield(sceneTree, "idle_frame")
		return
	AudioPlayer.fadeMusic(0.25, 0.4, false)
	sceneTree.paused = true
	AudioPlayer.playSound(music, 1, 1, "Music")
	var instance:CanvasLayer = scene.instance()
	instance.pause_mode = Node.PAUSE_MODE_PROCESS
	sceneTree.get_root().add_child(instance)
	instance.get_node("CenterContainer/NinePatchRect/Text").text = definition.text
	instance.get_node("CenterContainer/NinePatchRect/ShadowText").text = definition.text
	instance.get_node("CenterContainer/NinePatchRect/Description").text = definition.description
	var tween = instance.get_node("Tween")
	var container = instance.get_node("CenterContainer")
	container.rect_scale = Vector2(0, 0)
	tween.interpolate_property(container, "rect_scale", Vector2(0, 0), Vector2(1, 1), .2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(sceneTree.create_timer(1.0), "timeout")
	yield(Yields.inputPressed("ui_accept"), "completed")
	yield(instance.get_tree(), "idle_frame")
	tween.interpolate_property(container, "rect_scale", Vector2(1, 1), Vector2(0, 0), .2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()
	AudioPlayer.fadeMusic(0.4, 1, false)
	yield(tween, "tween_completed")
	instance.queue_free()
	sceneTree.paused = false
